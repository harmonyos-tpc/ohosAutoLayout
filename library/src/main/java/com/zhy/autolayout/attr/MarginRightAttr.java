package com.zhy.autolayout.attr;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;

/**
 * Created by zhy on 15/12/5.
 */
public class MarginRightAttr extends AutoAttr
{
    public MarginRightAttr(int pxVal, int baseWidth, int baseHeight)
    {
        super(pxVal, baseWidth, baseHeight);
    }

    @Override
    protected int attrVal()
    {
        return Attrs.MARGIN_RIGHT;
    }

    @Override
    protected boolean defaultBaseWidth()
    {
        return true;
    }

    @Override
    protected void execute(Component view, int val)
    {
        //CHANGE-TP2
        /*if (!(view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams))
        {
            return;
        }
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        lp.rightMargin = val;*/
        if (!(view.getLayoutConfig() instanceof ComponentContainer.LayoutConfig))
        {
            return;
        }
        ComponentContainer.LayoutConfig layoutConfig = view.getLayoutConfig();
        layoutConfig.setMarginRight(val);
        view.setLayoutConfig(layoutConfig);
    }


    public static MarginRightAttr generate(int val, int baseFlag)
    {
        MarginRightAttr attr = null;
        switch (baseFlag)
        {
            case BASE_WIDTH:
                attr = new MarginRightAttr(val, Attrs.MARGIN_RIGHT, 0);
                break;
            case BASE_HEIGHT:
                attr = new MarginRightAttr(val, 0, Attrs.MARGIN_RIGHT);
                break;
            case BASE_DEFAULT:
                attr = new MarginRightAttr(val, 0, 0);
                break;
        }
        return attr;
    }
}
