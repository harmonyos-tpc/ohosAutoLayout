package com.zhy.autolayout.attr;

import ohos.agp.components.Component;


/**
 * Created by zhy on 15/12/24.
 */
public class MinHeightAttr extends AutoAttr
{
    public MinHeightAttr(int pxVal, int baseWidth, int baseHeight)
    {
        super(pxVal, baseWidth, baseHeight);
    }

    @Override
    protected int attrVal()
    {
        return Attrs.MIN_HEIGHT;
    }

    @Override
    protected boolean defaultBaseWidth()
    {
        return false;
    }

    @Override
    protected void execute(Component view, int val)
    {
        try
        {
            view.setMinHeight(val);
//            Method setMaxWidthMethod = view.getClass().getMethod("setMinHeight", int.class);
//            setMaxWidthMethod.invoke(view, val);
        } catch (Exception ignore)
        {
        }
    }

    public static MinHeightAttr generate(int val, int baseFlag)
    {
        MinHeightAttr attr = null;
        switch (baseFlag)
        {
            case BASE_WIDTH:
                attr = new MinHeightAttr(val, Attrs.MIN_HEIGHT, 0);
                break;
            case BASE_HEIGHT:
                attr = new MinHeightAttr(val, 0, Attrs.MIN_HEIGHT);
                break;
            case BASE_DEFAULT:
                attr = new MinHeightAttr(val, 0, 0);
                break;
        }
        return attr;
    }

    public static int getMinHeight(Component view)
    {
        //CHANGE-TP3 忽略Build.VERSION
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
        {
            return view.getMinimumHeight();
        } else
        {
            try
            {
                Field minHeight = view.getClass().getField("mMinHeight");
                minHeight.setAccessible(true);
                return (int) minHeight.get(view);
            } catch (Exception e)
            {
            }
        }

        return 0;*/
        return view.getMinHeight();
    }

}
