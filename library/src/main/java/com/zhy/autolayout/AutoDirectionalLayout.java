package com.zhy.autolayout;

import com.zhy.autolayout.utils.AutoLayoutHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.render.Canvas;
import ohos.app.Context;

public class AutoDirectionalLayout extends DirectionalLayout {

    private AutoLayoutHelper mHelper = new AutoLayoutHelper(this);
    private boolean hasAdjust = false;

    public AutoDirectionalLayout(Context context) {
        super(context);
        init();
    }

    public AutoDirectionalLayout(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init();
    }

    public AutoDirectionalLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
    }

    private void init() {
       addDrawTask(new DrawTask() {
           @Override
           public void onDraw(Component component, Canvas canvas) {
               if (!hasAdjust && getChildCount() > 0) {
                   mHelper.adjustChildren();
                   hasAdjust = true;
               }
           }
       });

    }

    @Override
    public ComponentContainer.LayoutConfig createLayoutConfig(Context context, AttrSet attrSet) {
        return new AutoDirectionalLayout.LayoutConfig(context, attrSet);
    }

    public static class LayoutConfig extends DirectionalLayout.LayoutConfig implements AutoLayoutHelper.AutoLayoutParams {
        private AutoLayoutInfo mAutoLayoutInfo;

        public LayoutConfig(Context context, AttrSet attrSet) {
            super(context, attrSet);
            mAutoLayoutInfo = AutoLayoutHelper.getAutoLayoutInfo(context, attrSet);
        }

        @Override
        public AutoLayoutInfo getAutoLayoutInfo() {
            return mAutoLayoutInfo;
        }
    }

}
