package com.zhy.autolayout.attr;


import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;

/**
 * Created by zhy on 15/12/5.
 */
public class WidthAttr extends AutoAttr
{
    public WidthAttr(int pxVal, int baseWidth, int baseHeight)
    {
        super(pxVal, baseWidth, baseHeight);
    }

    @Override
    protected int attrVal()
    {
        return Attrs.WIDTH;
    }

    @Override
    protected boolean defaultBaseWidth()
    {
        return true;
    }

    @Override
    protected void execute(Component view, int val)
    {
        ComponentContainer.LayoutConfig lp = view.getLayoutConfig();
        lp.width = val;
        view.setLayoutConfig(lp);
    }

    public static WidthAttr generate(int val, int baseFlag)
    {
        WidthAttr widthAttr = null;
        switch (baseFlag)
        {
            case BASE_WIDTH:
                widthAttr = new WidthAttr(val, Attrs.WIDTH, 0);
                break;
            case BASE_HEIGHT:
                widthAttr = new WidthAttr(val, 0, Attrs.WIDTH);
                break;
            case BASE_DEFAULT:
                widthAttr = new WidthAttr(val, 0, 0);
                break;
        }
        return widthAttr;
    }

}
