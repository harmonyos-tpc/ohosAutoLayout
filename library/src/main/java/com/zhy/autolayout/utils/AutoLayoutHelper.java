/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zhy.autolayout.utils;


import com.zhy.autolayout.AutoLayoutInfo;
import com.zhy.autolayout.attr.*;
import com.zhy.autolayout.config.AutoLayoutConfig;
import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.app.Context;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class AutoLayoutHelper
{
    private final ComponentContainer mHost;

    private static final Map<String,Integer> map = new HashMap<>();
            {
                map.put("text_size",INDEX_TEXT_SIZE);
                map.put("padding",INDEX_PADDING);
                map.put("left_padding",INDEX_PADDING_LEFT);
                map.put("top_padding",INDEX_PADDING_TOP);
                map.put("right_padding",INDEX_PADDING_RIGHT);
                map.put("bottom_padding",INDEX_PADDING_BOTTOM);
                map.put("width",INDEX_WIDTH);
                map.put("height",INDEX_HEIGHT);
                map.put("margin",INDEX_MARGIN);
                map.put("left_margin",INDEX_MARGIN_LEFT);
                map.put("top_margin",INDEX_MARGIN_TOP);
                map.put("right_margin",INDEX_MARGIN_RIGHT);
                map.put("bottom_margin",INDEX_MARGIN_BOTTOM);
                map.put("maxWidth",INDEX_MAX_WIDTH);
                map.put("maxHeight",INDEX_MAX_HEIGHT);
                map.put("minWidth",INDEX_MIN_WIDTH);
                map.put("minHeight",INDEX_MIN_HEIGHT);
            };

    private static final int INDEX_TEXT_SIZE = 0;
    private static final int INDEX_PADDING = 1;
    private static final int INDEX_PADDING_LEFT = 2;
    private static final int INDEX_PADDING_TOP = 3;
    private static final int INDEX_PADDING_RIGHT = 4;
    private static final int INDEX_PADDING_BOTTOM = 5;
    private static final int INDEX_WIDTH = 6;
    private static final int INDEX_HEIGHT = 7;
    private static final int INDEX_MARGIN = 8;
    private static final int INDEX_MARGIN_LEFT = 9;
    private static final int INDEX_MARGIN_TOP = 10;
    private static final int INDEX_MARGIN_RIGHT = 11;
    private static final int INDEX_MARGIN_BOTTOM = 12;
    private static final int INDEX_MAX_WIDTH = 13;
    private static final int INDEX_MAX_HEIGHT = 14;
    private static final int INDEX_MIN_WIDTH = 15;
    private static final int INDEX_MIN_HEIGHT = 16;


    /**
     * move to other place?
     */
    private static AutoLayoutConfig mAutoLayoutConfig;

    public AutoLayoutHelper(ComponentContainer host)
    {
        mHost = host;

        if (mAutoLayoutConfig == null)
        {
            initAutoLayoutConfig(host);
        }

    }

    private void initAutoLayoutConfig(ComponentContainer host)
    {
        mAutoLayoutConfig = AutoLayoutConfig.getInstance();
        mAutoLayoutConfig.init(host.getContext());
    }


    public void adjustChildren()
    {
        AutoLayoutConfig.getInstance().checkParams();
        for (int i = 0, n = mHost.getChildCount(); i < n; i++)
        {
            Component view = mHost.getComponentAt(i);
            ComponentContainer.LayoutConfig params = view.getLayoutConfig();

            if (params instanceof AutoLayoutParams)
            {
                AutoLayoutInfo info =
                        ((AutoLayoutParams) params).getAutoLayoutInfo();
                if (info != null)
                {
                    info.fillAttrs(view);
                }
            }
        }

    }

    public static AutoLayoutInfo getAutoLayoutInfo(Context context,
                                                   AttrSet attrs)
    {

        AutoLayoutInfo info = new AutoLayoutInfo();
       /* TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.AutoLayout_Layout);
        int baseWidth = a.getInt(R.styleable.AutoLayout_Layout_layout_auto_basewidth, 0);
        int baseHeight = a.getInt(R.styleable.AutoLayout_Layout_layout_auto_baseheight, 0);
        a.recycle();

        TypedArray array = context.obtainStyledAttributes(attrs, LL);

        int n = array.getIndexCount();*/


       int baseWidth = 0;
       int baseHeight = 0;
        if (attrs.getAttr("base_width").isPresent()) {
            baseWidth = attrs.getAttr("base_width").get().getIntegerValue();
        }
        if (attrs.getAttr("base_height").isPresent()) {
            baseHeight = attrs.getAttr("base_height").get().getIntegerValue();
        }


        for (int i = 0; i < attrs.getLength(); i++)
        {
            Attr attr = attrs.getAttr(i).get();

//            String val = array.getString(index);
//            if (!isPxVal(val)) continue;

            //CHANGE-TP6 没找到判断dimens单位的方法，先注释
            /*if (!DimenUtils.isPxVal(array.peekValue(index))) continue;*/

            int pxVal = 0;
            try
            {
                //pxVal = array.getDimensionPixelOffset(index, 0);
                pxVal = attr.getDimensionValue();
            } catch (Exception ignore)//not dimension
            {
                continue;
            }
            Logger.getLogger("AutoLayout").info(attr.getName());
            Integer index = map.get(attr.getName());
            if (index == null) {
                Logger.getLogger("AutoLayout").info(attr.getName()+ "不在映射表里面");
                continue;
            }
            switch(index)
            {
                case INDEX_TEXT_SIZE:
                    info.addAttr(new TextSizeAttr(pxVal, baseWidth, baseHeight));
                    break;
                case INDEX_PADDING:
                    info.addAttr(new PaddingAttr(pxVal, baseWidth, baseHeight));
                    break;
                case INDEX_PADDING_LEFT:
                    info.addAttr(new PaddingLeftAttr(pxVal, baseWidth, baseHeight));
                    break;
                case INDEX_PADDING_TOP:
                    info.addAttr(new PaddingTopAttr(pxVal, baseWidth, baseHeight));
                    break;
                case INDEX_PADDING_RIGHT:
                    info.addAttr(new PaddingRightAttr(pxVal, baseWidth, baseHeight));
                    break;
                case INDEX_PADDING_BOTTOM:
                    info.addAttr(new PaddingBottomAttr(pxVal, baseWidth, baseHeight));
                    break;
                case INDEX_WIDTH:
                    info.addAttr(new WidthAttr(pxVal, baseWidth, baseHeight));
                    break;
                case INDEX_HEIGHT:
                    info.addAttr(new HeightAttr(pxVal, baseWidth, baseHeight));
                    break;
                case INDEX_MARGIN:
                    info.addAttr(new MarginAttr(pxVal, baseWidth, baseHeight));
                    break;
                case INDEX_MARGIN_LEFT:
                    info.addAttr(new MarginLeftAttr(pxVal, baseWidth, baseHeight));
                    break;
                case INDEX_MARGIN_TOP:
                    info.addAttr(new MarginTopAttr(pxVal, baseWidth, baseHeight));
                    break;
                case INDEX_MARGIN_RIGHT:
                    info.addAttr(new MarginRightAttr(pxVal, baseWidth, baseHeight));
                    break;
                case INDEX_MARGIN_BOTTOM:
                    info.addAttr(new MarginBottomAttr(pxVal, baseWidth, baseHeight));
                    break;
                case INDEX_MAX_WIDTH:
                    info.addAttr(new MaxWidthAttr(pxVal, baseWidth, baseHeight));
                    break;
                case INDEX_MAX_HEIGHT:
                    info.addAttr(new MaxHeightAttr(pxVal, baseWidth, baseHeight));
                    break;
                case INDEX_MIN_WIDTH:
                    info.addAttr(new MinWidthAttr(pxVal, baseWidth, baseHeight));
                    break;
                case INDEX_MIN_HEIGHT:
                    info.addAttr(new MinHeightAttr(pxVal, baseWidth, baseHeight));
                    break;
            }
        }
        //array.recycle();
        L.e(" getAutoLayoutInfo " + info.toString());
        return info;
    }

    public interface AutoLayoutParams
    {
        AutoLayoutInfo getAutoLayoutInfo();
    }
}
