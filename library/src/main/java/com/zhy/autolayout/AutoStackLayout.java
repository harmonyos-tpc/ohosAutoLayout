package com.zhy.autolayout;

import com.zhy.autolayout.utils.AutoLayoutHelper;
import ohos.agp.components.*;
import ohos.agp.render.Canvas;
import ohos.app.Context;

public class AutoStackLayout extends StackLayout {

    private AutoLayoutHelper mHelper = new AutoLayoutHelper(this);
    private boolean hasAdjust = false;

    public AutoStackLayout(Context context) {
        super(context);
        init();
    }

    public AutoStackLayout(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init();
    }

    public AutoStackLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
    }

    private void init() {
       addDrawTask(new DrawTask() {
           @Override
           public void onDraw(Component component, Canvas canvas) {
               if (!hasAdjust && getChildCount() > 0) {
                   mHelper.adjustChildren();
                   hasAdjust = true;
               }
           }
       });

    }

    @Override
    public ComponentContainer.LayoutConfig createLayoutConfig(Context context, AttrSet attrSet) {
        return new AutoStackLayout.LayoutConfig(context, attrSet);
    }

    public static class LayoutConfig extends StackLayout.LayoutConfig implements AutoLayoutHelper.AutoLayoutParams {
        private AutoLayoutInfo mAutoLayoutInfo;

        public LayoutConfig(Context context, AttrSet attrSet) {
            super(context, attrSet);
            mAutoLayoutInfo = AutoLayoutHelper.getAutoLayoutInfo(context, attrSet);
        }

        @Override
        public AutoLayoutInfo getAutoLayoutInfo() {
            return mAutoLayoutInfo;
        }
    }

}
