package com.zhy.autolayout.config;


import com.zhy.autolayout.utils.L;
import com.zhy.autolayout.utils.ScreenUtils;
import ohos.app.Context;

/**
 * Created by zhy on 15/11/18.
 */
public class AutoLayoutConfig
{

    private static AutoLayoutConfig sInstance = new AutoLayoutConfig();


    private static final String KEY_DESIGN_WIDTH = "design_width";
    private static final String KEY_DESIGN_HEIGHT = "design_height";

    private int mScreenWidth;
    private int mScreenHeight;

    private int mDesignWidth = 1080;
    private int mDesignHeight = 1920;

    private boolean useDeviceSize;


    private AutoLayoutConfig()
    {
    }

    public void checkParams()
    {
        if (mDesignHeight <= 0 || mDesignWidth <= 0)
        {
            throw new RuntimeException(
                    "you must set " + KEY_DESIGN_WIDTH + " and " + KEY_DESIGN_HEIGHT + "  in your manifest file.");
        }
    }

    public AutoLayoutConfig useDeviceSize()
    {
        useDeviceSize = true;
        return this;
    }


    public static AutoLayoutConfig getInstance()
    {
        return sInstance;
    }


    public int getScreenWidth()
    {
        return mScreenWidth;
    }

    public int getScreenHeight()
    {
        return mScreenHeight;
    }

    public int getDesignWidth()
    {
        return mDesignWidth;
    }

    public int getDesignHeight()
    {
        return mDesignHeight;
    }


    public void init(Context context)
    {
        //getMetaData(context);

        int[] screenSize = ScreenUtils.getScreenSize(context, useDeviceSize);
        mScreenWidth = screenSize[0];
        mScreenHeight = screenSize[1];
        L.e(" screenWidth =" + mScreenWidth + " ,screenHeight = " + mScreenHeight);
    }

    public void setDesignSize(int designWidth,int designHeight) {
        mDesignWidth = designWidth;
        mDesignHeight = designHeight;
    }

    private void getMetaData(Context context)
    {
        //CHANGE-TP4 之前从metaData里读取,现在先写死
        /*PackageManager packageManager = context.getPackageManager();
        ApplicationInfo applicationInfo;
        try
        {
            applicationInfo = packageManager.getApplicationInfo(context
                    .getPackageName(), PackageManager.GET_META_DATA);
            if (applicationInfo != null && applicationInfo.metaData != null)
            {
                mDesignWidth = (int) applicationInfo.metaData.get(KEY_DESIGN_WIDTH);
                mDesignHeight = (int) applicationInfo.metaData.get(KEY_DESIGN_HEIGHT);
            }
        } catch (PackageManager.NameNotFoundException e)
        {
            throw new RuntimeException(
                    "you must set " + KEY_DESIGN_WIDTH + " and " + KEY_DESIGN_HEIGHT + "  in your manifest file.", e);
        }

        L.e(" designWidth =" + mDesignWidth + " , designHeight = " + mDesignHeight);*/
    }


}
