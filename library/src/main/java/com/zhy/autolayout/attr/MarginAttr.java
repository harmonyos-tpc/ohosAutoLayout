package com.zhy.autolayout.attr;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;

/**
 * Created by zhy on 15/12/5.
 */
public class MarginAttr extends AutoAttr
{
    public MarginAttr(int pxVal, int baseWidth, int baseHeight)
    {
        super(pxVal, baseWidth, baseHeight);
    }

    @Override
    protected int attrVal()
    {
        return Attrs.MARGIN;
    }

    @Override
    protected boolean defaultBaseWidth()
    {
        return false;
    }

    @Override
    public void apply(Component view)
    {
        //CHANGE-TP2 ComponentContainer.LayoutConfig自带margin属性，不需要强转
       /* if (!(view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams))
        {
            return;
        }
        if (useDefault())
        {
            ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            lp.leftMargin = lp.rightMargin = getPercentWidthSize();
            lp.topMargin = lp.bottomMargin = getPercentHeightSize();
            return;
        }
        super.apply(view);*/
        if (!(view.getLayoutConfig() instanceof ComponentContainer.LayoutConfig))
        {
            return;
        }
        if (useDefault())
        {
            ComponentContainer.LayoutConfig layoutConfig = view.getLayoutConfig();
            layoutConfig.setMargins(getPercentWidthSize(),getPercentHeightSize(),getPercentWidthSize(),getPercentHeightSize());
            view.setLayoutConfig(layoutConfig);
            return;
        }
        super.apply(view);
    }

    @Override
    protected void execute(Component view, int val)
    {
        //CHANGE-TP2
        /*ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        lp.leftMargin = lp.rightMargin = lp.topMargin = lp.bottomMargin = val;*/
        ComponentContainer.LayoutConfig layoutConfig = view.getLayoutConfig();
        layoutConfig.setMargins(val,val,val,val);
    }

}
