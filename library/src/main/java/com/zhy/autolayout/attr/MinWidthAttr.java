package com.zhy.autolayout.attr;


import ohos.agp.components.Component;

/**
 * Created by zhy on 15/12/24.
 */
public class MinWidthAttr extends AutoAttr
{
    public MinWidthAttr(int pxVal, int baseWidth, int baseHeight)
    {
        super(pxVal, baseWidth, baseHeight);
    }

    @Override
    protected int attrVal()
    {
        return Attrs.MIN_WIDTH;
    }

    @Override
    protected boolean defaultBaseWidth()
    {
        return true;
    }

    @Override
    protected void execute(Component view, int val)
    {
        try
        {
//            Method setMaxWidthMethod = view.getClass().getMethod("setMinWidth", int.class);
//            setMaxWidthMethod.invoke(view, val);
        } catch (Exception ignore)
        {
        }

        view.setMinWidth(val);
    }

    public static int getMinWidth(Component view)
    {
        //CHANGE-TP3 忽略Build.VERSION
       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            return view.getMinimumWidth();
        try
        {
            Field minWidth = view.getClass().getField("mMinWidth");
            minWidth.setAccessible(true);
            return (int) minWidth.get(view);
        } catch (Exception ignore)
        {
        }
        return 0;*/
       return view.getMinWidth();
    }


    public static MinWidthAttr generate(int val, int baseFlag)
    {
        MinWidthAttr attr = null;
        switch (baseFlag)
        {
            case BASE_WIDTH:
                attr = new MinWidthAttr(val, Attrs.MIN_WIDTH, 0);
                break;
            case BASE_HEIGHT:
                attr = new MinWidthAttr(val, 0, Attrs.MIN_WIDTH);
                break;
            case BASE_DEFAULT:
                attr = new MinWidthAttr(val, 0, 0);
                break;
        }
        return attr;
    }
}
