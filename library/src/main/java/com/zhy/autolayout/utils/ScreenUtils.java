package com.zhy.autolayout.utils;


import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

/**
 * Created by zhy on 15/12/4.<br/>
 * form http://stackoverflow.com/questions/1016896/get-screen-dimensions-in-pixels/15699681#15699681
 */
public class ScreenUtils
{

    public static int getStatusBarHeight(Context context)
    {
        //CHANGE-TP6 获取状态栏高度api缺失，暂时写死
       // (540 - 506) * 2 = 68px
       return 68;
    }


    public static int[] getScreenSize(Context context, boolean useDeviceSize)
    {

       /* int[] size = new int[2];

        WindowManager w = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display d = w.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        d.getMetrics(metrics);
// since SDK_INT = 1;
        int widthPixels = metrics.widthPixels;
        int heightPixels = metrics.heightPixels;

        if (!useDeviceSize)
        {
            size[0] = widthPixels;
            size[1] = heightPixels - getStatusBarHeight(context);

            return size;
        }

// includes window decorations (statusbar bar/menu bar)
        if (Build.VERSION.SDK_INT >= 14 && Build.VERSION.SDK_INT < 17)
            try
            {
                widthPixels = (Integer) Display.class.getMethod("getRawWidth").invoke(d);
                heightPixels = (Integer) Display.class.getMethod("getRawHeight").invoke(d);
            } catch (Exception ignored)
            {
            }
// includes window decorations (statusbar bar/menu bar)
        if (Build.VERSION.SDK_INT >= 17)
            try
            {
                Point realSize = new Point();
                Display.class.getMethod("getRealSize", Point.class).invoke(d, realSize);
                widthPixels = realSize.x;
                heightPixels = realSize.y;
            } catch (Exception ignored)
            {
            }
        size[0] = widthPixels;
        size[1] = heightPixels;
        return size;*/
        int[] size = new int[2];
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        DisplayAttributes realAttributes = display.getRealAttributes();
        size[0] = realAttributes.width;
        size[1] = useDeviceSize? realAttributes.height : realAttributes.height - getStatusBarHeight(context);
        return size;
    }

}
