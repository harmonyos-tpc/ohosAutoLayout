/**
 * Copyright 2021 OpenHarmony
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zhy.autolayout.demo.slice;

import com.zhy.autolayout.demo.ListItemProvider;
import com.zhy.autolayout.demo.ResourceTable;
import com.zhy.autolayout.demo.SamplePageSliderProvider;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

import java.util.ArrayList;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);

        ComponentContainer root = (ComponentContainer) LayoutScatter.getInstance(this).parse(ResourceTable.Layout_activity_main, null, false);
        PageSlider pageSlider = (PageSlider) root.findComponentById(ResourceTable.Id_pageSlider);
        List<Component> lists = new ArrayList<>();
        Component fragment_list = LayoutScatter.getInstance(this).parse(ResourceTable.Layout_fragment_list, null, false);
        ListContainer listContainer = (ListContainer) fragment_list.findComponentById(ResourceTable.Id_listContainer);
        listContainer.setItemProvider(new ListItemProvider(this));

        Component fragment_register = LayoutScatter.getInstance(this).parse(ResourceTable.Layout_fragment_register, null, false);
        Component fragment_pay = LayoutScatter.getInstance(this).parse(ResourceTable.Layout_fragment_pay, null, false);
        Component fragment_test = LayoutScatter.getInstance(this).parse(ResourceTable.Layout_fragment_test, null, false);

        lists.add(fragment_list);
        lists.add(fragment_register);
        lists.add(fragment_pay);
        lists.add(fragment_test);
        pageSlider.setProvider(new SamplePageSliderProvider(lists));
        setUIContent(root);
    }

}
