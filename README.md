## ohosAutoLayout
屏幕适配方案，直接填写设计图上的像素尺寸即可完成适配。

## Usage
Solution 1: 
local har package integration
1.Add the har package to the lib folder.
2.Add the following code to gradle of the entry:
```gradle
implementation fileTree(dir: 'libs', include: ['.jar', '.har'])
```
Solution 2:
```gradle
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:ohosAutoLayout:1.0.2'
```

## entry运行要求
通过DevEco studio,并下载OpenHarmonySDK
将项目中的build.gradle文件中dependencies→classpath版本改为对应的版本（即你的IDE新建项目中所用的版本）

## Screenshot
![ohosautolayout.gif](ohosautolayout.gif)

## 用法

### 第一步：

初始化使用屏幕物理尺寸以及设计稿对应的宽高像素值

```java
 AutoLayoutConfig.getInstance().useDeviceSize().setDesignSize(1920,1080);
    
```

### 第二步：

在编写布局文件时，将

* DirectionalLayout -> AutoDirectionalLayout
* DependentLayout -> AutoDependentLayout
* StackLayout -> AutoStackLayout


## 目前支持属性

* width
* height
* margin(left,top,right,bottom)
* padding(left,top,right,bottom)
* text_size
* maxWidth, minWidth, maxHeight, minHeight


## 扩展

对于其他继承系统的DirectionalLayout、DependentLayout、StackLayout，如果希望再其内部直接支持"px"百分比化，可以自己扩展，具体查看
AutoDirectionalLayout的源码


### 指定设置的值参考宽度或者高度

由于该库的特点，布局文件中宽高上的1px是不相等的，于是如果需要宽高保持一致的情况，布局中使用属性：

 `app:base_width="10"`，代表height上编写的像素值参考宽度。
 
 `app:base_height="1"`，代表width上编写的像素值参考高度。

如果需要指定多个值参考宽度即：

`app:base_height="101"`表示宽度和字体大小跟随高度变化,具体参见Attrs这个类


### TextView的高度问题

设计稿一般只会标识一个字体的大小，比如你设置textSize="20px"，实际上TextView所占据的高度肯定大于20px，字的上下都会有一定的间隙，所以一定要灵活去写字体的高度，比如对于text上下的margin可以选择尽可能小一点。或者选择别的约束条件去定位（比如上例，选择了marginBottom）
