package com.zhy.autolayout.utils;


import java.util.logging.Logger;

/**
 * Created by zhy on 15/11/18.
 */
public class L {
    public static boolean debug = true;
    private static final String TAG = "AutoLayout";

    public static void e(String msg) {
        if (debug) {
            Logger.getLogger(TAG).warning(msg);
        }
    }


}
