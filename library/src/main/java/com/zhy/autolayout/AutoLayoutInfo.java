package com.zhy.autolayout;

import com.zhy.autolayout.attr.*;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Text;

import java.util.ArrayList;
import java.util.List;

public class AutoLayoutInfo
{
    private List<AutoAttr> autoAttrs = new ArrayList<>();

    public void addAttr(AutoAttr autoAttr)
    {
        autoAttrs.add(autoAttr);
    }


    public void fillAttrs(Component view)
    {
        for (AutoAttr autoAttr : autoAttrs)
        {
            autoAttr.apply(view);
        }
    }


    public static AutoLayoutInfo getAttrFromView(Component view, int attrs, int base)
    {
        ComponentContainer.LayoutConfig params = view.getLayoutConfig();
        if (params == null) return null;
        AutoLayoutInfo autoLayoutInfo = new AutoLayoutInfo();

        // width & height
        if ((attrs & Attrs.WIDTH) != 0 && params.width > 0)
        {
            autoLayoutInfo.addAttr(WidthAttr.generate(params.width, base));
        }

        if ((attrs & Attrs.HEIGHT) != 0 && params.height > 0)
        {
            autoLayoutInfo.addAttr(HeightAttr.generate(params.height, base));
        }

        //margin
        if (params instanceof ComponentContainer.LayoutConfig)
        {
            if ((attrs & Attrs.MARGIN) != 0)
            {
                autoLayoutInfo.addAttr(MarginLeftAttr.generate(((ComponentContainer.LayoutConfig) params).getMarginLeft(), base));
                autoLayoutInfo.addAttr(MarginTopAttr.generate(((ComponentContainer.LayoutConfig) params).getMarginTop(), base));
                autoLayoutInfo.addAttr(MarginRightAttr.generate(((ComponentContainer.LayoutConfig) params).getMarginRight(), base));
                autoLayoutInfo.addAttr(MarginBottomAttr.generate(((ComponentContainer.LayoutConfig) params).getMarginBottom(), base));
            }
            if ((attrs & Attrs.MARGIN_LEFT) != 0)
            {
                autoLayoutInfo.addAttr(MarginLeftAttr.generate(((ComponentContainer.LayoutConfig) params).getMarginLeft(), base));
            }
            if ((attrs & Attrs.MARGIN_TOP) != 0)
            {
                autoLayoutInfo.addAttr(MarginTopAttr.generate(((ComponentContainer.LayoutConfig) params).getMarginTop(), base));
            }
            if ((attrs & Attrs.MARGIN_RIGHT) != 0)
            {
                autoLayoutInfo.addAttr(MarginRightAttr.generate(((ComponentContainer.LayoutConfig) params).getMarginRight(), base));
            }
            if ((attrs & Attrs.MARGIN_BOTTOM) != 0)
            {
                autoLayoutInfo.addAttr(MarginBottomAttr.generate(((ComponentContainer.LayoutConfig) params).getMarginBottom(), base));
            }
        }

        //padding
        if ((attrs & Attrs.PADDING) != 0)
        {
            autoLayoutInfo.addAttr(PaddingLeftAttr.generate(view.getPaddingLeft(), base));
            autoLayoutInfo.addAttr(PaddingTopAttr.generate(view.getPaddingTop(), base));
            autoLayoutInfo.addAttr(PaddingRightAttr.generate(view.getPaddingRight(), base));
            autoLayoutInfo.addAttr(PaddingBottomAttr.generate(view.getPaddingBottom(), base));
        }
        if ((attrs & Attrs.PADDING_LEFT) != 0)
        {
            autoLayoutInfo.addAttr(MarginLeftAttr.generate(view.getPaddingLeft(), base));
        }
        if ((attrs & Attrs.PADDING_TOP) != 0)
        {
            autoLayoutInfo.addAttr(MarginTopAttr.generate(view.getPaddingTop(), base));
        }
        if ((attrs & Attrs.PADDING_RIGHT) != 0)
        {
            autoLayoutInfo.addAttr(MarginRightAttr.generate(view.getPaddingRight(), base));
        }
        if ((attrs & Attrs.PADDING_BOTTOM) != 0)
        {
            autoLayoutInfo.addAttr(MarginBottomAttr.generate(view.getPaddingBottom(), base));
        }

        //minWidth ,maxWidth , minHeight , maxHeight
        if ((attrs & Attrs.MIN_WIDTH) != 0)
        {
            autoLayoutInfo.addAttr(MinWidthAttr.generate(MinWidthAttr.getMinWidth(view), base));
        }
        if ((attrs & Attrs.MAX_WIDTH) != 0)
        {
            autoLayoutInfo.addAttr(MaxWidthAttr.generate(MaxWidthAttr.getMaxWidth(view), base));
        }
        if ((attrs & Attrs.MIN_HEIGHT) != 0)
        {
            autoLayoutInfo.addAttr(MinHeightAttr.generate(MinHeightAttr.getMinHeight(view), base));
        }
        if ((attrs & Attrs.MAX_HEIGHT) != 0)
        {
            autoLayoutInfo.addAttr(MaxHeightAttr.generate(MaxHeightAttr.getMaxHeight(view), base));
        }

        //textsize

        if (view instanceof Text)
        {
            if ((attrs & Attrs.TEXTSIZE) != 0)
            {
                autoLayoutInfo.addAttr(TextSizeAttr.generate((int) ((Text) view).getTextSize(), base));
            }
        }
        return autoLayoutInfo;
    }


    @Override
    public String toString()
    {
        return "AutoLayoutInfo{" +
                "autoAttrs=" + autoAttrs +
                '}';
    }
}